'use strict';
////////////////////////////////////////////////////////////////////////////////
//	Routes for handling API authentication requests:
//	Intercepts the  authentication requests and calls the
//	controller "auth.server.controller" for authentication.
////////////////////////////////////////////////////////////////////////////////
module.exports = function(app) {
	var authcontroller = require('../controllers/auth.server.controller.js');

	//app.post('/api/authenticate', authcontroller.authenticate);
	app.post('/pctserver/api/', authcontroller.authenticate);
}