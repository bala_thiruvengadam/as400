///////////////////////////////////////////////////////////////////////////
///   UserPrincipal model
///////////////////////////////////////////////////////////////////////////
var UserPrincipal = function(username, password) {
	this.username = username;
	this.password = '';
	this.lastUpdated = new Date();
}

module.exports = {
	newUserPrincipal: function(username,password) {
		return new UserPrincipal(username, '');
	}
}
