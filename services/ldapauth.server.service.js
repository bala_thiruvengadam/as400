var ldap = require('ldapjs'),
  async = require('async'),
  logger = require('../utils/logger.server.util'),
  principal = require('../models/UserPrincipal'),
  config    = require('../config/settings.server.config.js').configs;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

function destroyLdapClient(ldapclient) {
  if (!ldapclient) {
    return
  } else {
    ldapclient.unbind();
    ldapclient.destroy();
    return;
  }
}

function getldapConfig(tempStore, callBack) {
  tempStore.ldapConfig = config.ldapAuthConfig;
  tempStore.authTimeout = config.authTimeout;
  return callBack(null, tempStore);
}


function findUserWithAdminAccount(tempStore, callBack) {
  var config = tempStore.ldapConfig,
    username = tempStore.username,
    password = tempStore.password;

  try {
    // Use the administrative account to find the user
    var adminClient = ldap.createClient({
      url: config.ldapUrl,
      //queueDisable: true,
      timeout: tempStore.authTimeout,
      connectTimeout: tempStore.authTimeout
    });
    // Bind as the administrator (or a read-only user), to get the DN for the user attempting to authenticate
    adminClient.bind(config.ldapAdminUserDN, config.ldapAdminUserPassword,
      function(err) {
        if (err) {
          logger.error(__filename, "Error binding admin user : " + err);
          return callBack(err, null);
        }
        // Search for a user
        adminClient.search(config.ldapAdminUserBase, {
          scope: "sub",
          filter: "(" + config.ldapUserNameAttr + "=" +
            username + ")"
        }, function(err, ldapResult) {
          if (err) {
            logger.error(__filename,
              "ERROR searching userNameAttr : " +
              err);
            return callBack(err, null);
          }
          ldapResult.on('searchEntry', function(entry) {
            return callBack(null, tempStore,
              entry);
          });
        });
      });
  } catch (err) {
    logger.error(__filename, "Error connecting to LDAP server : ", err);
    return callBack(err);
  }
}

function authenticateUser(tempStore, entry, callBack) {
  var username = tempStore.username,
    password = tempStore.password,
    config = tempStore.ldapConfig;
  try {
    var dn = entry.dn;
    logger.debug(__filename, "- DN=" + dn);
    // When you have the DN, try to bind with it to check the password
    var userClient = ldap.createClient({
      url: config.ldapUrl
    });
    userClient.bind(dn, password, function(err, user) {
      if (err) {
        logger.warn(__filename, "- Authentication failed: " + err);
        return callBack(err, null);
      }
      logger.debug(__filename, "- Successfully authenticated !");
      return callBack(null, tempStore, entry);
    });
  } catch (err) {
    logger.error(__filename, "- Error while authenticating: ", err);
    return callBack(err, null);
  }

}


exports.authenticate = function(tempStore, callBack) {
    async.waterfall([
        async.apply(getldapConfig, tempStore),
        findUserWithAdminAccount,
        authenticateUser
      ],
      function(err, tempStore, userNameAttr) {
        if (err) {
          return callBack(err);
        }
        // Create uUserPrincipal Object and return
        var userPrincipal = principal.newUserPrincipal(
          tempStore.username,
          '');
        tempStore.userPrincipal = userPrincipal;
        return callBack(null, tempStore);
      });  
}


