const winston = require('winston');
const logDir = 'log';

winston.emitErrs = true;


var logger = new winston.Logger({
  transports: [
    new winston.transports.Console({
      level: 'info',
      handleExceptions: true,
      json: false,
      timestamp: function() {
        return new Date();
      },
      formatter: function(options) {
        // Return string will be passed to logger.
        return options.timestamp() + ' ' + options.level.toUpperCase() + ': ' +
          (options.message ? options.message : '') +
          (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(
            options.meta) : '');
      },
      colorize: true
    })
  ],
  exitOnError: false
});



module.exports = logger;

module.exports.stream = {
  write: function(message, encoding) {
    logger.info(message);
  }
}
